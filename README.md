# zshrc

#### 项目介绍
zsh的配置文件
修改自https://github.com/skywind3000/vim/blob/master/etc/zshrc.zsh  
lscolor来自deepin系统  



#### 安装教程
##### zshrc.zsh
首先安装zsh, oh-my-zsh  
然后将zshrc.zsh的内容复制到~/.zshrc中
##### lscolor
###### 使用上面的zsh配置  
echo 'eval $(dircolors -b /etc/lscolor-256color)' >> ~/.antigen/bundles/robbyrussell/oh-my-zsh/lib/theme-and-appearance.zsh  
###### 使用bash  
echo 'eval $(dircolors -b /etc/lscolor-256color)' >> ~/.bashrc

上面的/etc/lscolor-256color 换成你下载该文件保存的目录 

